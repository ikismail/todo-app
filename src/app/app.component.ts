import { UserService } from "./components/users/shared/user.service";
import { Toast } from "angular2-toaster/src/toast";
import { Component, OnInit } from "@angular/core";
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { Ng2DeviceService } from "ng2-device-detector";
import { BodyOutputType } from "angular2-toaster/src/bodyOutputType";
import { toasterconfig } from "../environments/environment";
import { NativeNotificationService } from "angular-notice/lib/native-notification.service";
import { timeout } from "q";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  providers: [ToasterService],
  moduleId: module.id
})
export class AppComponent implements OnInit {
  deviceInfo = null;

  toasterconfig: ToasterConfig = toasterconfig;

  constructor(
    private toasterService: ToasterService,
    private deviceService: Ng2DeviceService,
    private noticeService: NativeNotificationService,
    private userService: UserService
  ) {
    this.deviceInfo = this.deviceService.getDeviceInfo();

    const toast: Toast = {
      type: "info",
      title: "Device Info",
      body:
        "Browser: " +
        this.deviceInfo.browser +
        "<br>" +
        "OS: " +
        this.deviceInfo.os,
      bodyOutputType: BodyOutputType.TrustedHtml,
      timeout: 2000
    };

    this.toasterService.popAsync(toast);
  }

  ngOnInit() {
    // GeoLocation getting current Locatino Lattitude and Longitude
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.setGeoLocation.bind(this));
    }

    // Web Push Notification
    const options = {
      title: "hello world",
      body: "this is a notification body",
      dir: "ltr",
      icon: "../favicon.ico",
      tag: "notice",
      closeDelay: 4000
    };
    this.noticeService.notify(options);
  }

  setGeoLocation(position: any) {
    this.userService.setLocation(
      position["coords"].latitude,
      position["coords"].longitude
    );
  }
}
