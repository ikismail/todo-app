import { UserService } from "./../../users/shared/user.service";
import { Login } from "./login.model";
import { Injectable } from "@angular/core";
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from "firebase/app";
import { Observable } from "rxjs/Observable";
import { log } from "util";
import { Buffer } from "buffer";
import { User } from "../../users/shared/user";

@Injectable()
export class AuthServiceService {
  user: Observable<firebase.User>;
  usersList: User[] = [];

  constructor(
    private firebaseAuth: AngularFireAuth,
    private userService: UserService
  ) {
    this.user = firebaseAuth.authState;
    this.getAllUsers();
  }

  signup(email: string, password: string) {
    this.firebaseAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then(value => {
        console.log("Success!", value);
      })
      .catch(err => {
        console.log("Something went wrong:", err.message);
      });
  }

  login(email: string, password: string): boolean {
    // this.firebaseAuth
    //   .auth
    //   .signInWithEmailAndPassword(email, password)
    //   .then(value => {
    //     this.user = value;
    //     this.userEmail = value.email;
    //     console.log('Nice, it worked!', this.user);
    //   })
    //   .catch(err => {
    //     console.log('Something went wrong:', err.message);
    //   });
    let status = false;

    this.usersList.forEach(user => {
      if (email === user.emailId && password === user.password) {
        const loggedInUser = user;
        const objStr = JSON.stringify(loggedInUser);
        const token = new Buffer(objStr).toString("base64");
        localStorage.setItem("token", token);
        sessionStorage.setItem("token", token);
        status = true;
        return status;
        // Decoder
        // const jsonB64Str = new Buffer(objJsonB64 || '', 'base64').toString('utf8');
      }

      return status;
    });

    return status;
  }

  logout() {
    // localStorage
    // this.firebaseAuth
    //   .auth
    //   .signOut();
    localStorage.removeItem("token");
    sessionStorage.removeItem("token");
  }

  isLoggedIn(): Boolean {
    // const token = localStorage.getItem("token");
    const token = sessionStorage.getItem("token");
    if (token) {
      return true;
    }
    return false;
  }

  isAdmin(): boolean {
    const token = sessionStorage.getItem("token");

    if (!token) {
      return false;
    }

    const strObj = new Buffer(token || "", "base64").toString("utf8");
    const loggedUser = JSON.parse(strObj);
    if (loggedUser["isAdmin"] === true) {
      return true;
    }
    return false;
  }

  getLoggedInUser(): User {
    const token = sessionStorage.getItem("token");

    if (!token) {
      return null;
    }

    const strObj = new Buffer(token || "", "base64").toString("utf8");
    const loggedUser: User = JSON.parse(strObj);
    return loggedUser;
  }

  getAllUsers() {
    const x = this.userService.getUsers();
    x.snapshotChanges().subscribe(user => {
      this.usersList = [];
      user.forEach(element => {
        const y = element.payload.toJSON();
        y["$key"] = element.key;
        this.usersList.push(y as User);
      });
    });
  }
}
