import {Component, OnInit} from "@angular/core";
import {AuthServiceService} from "../shared/auth-service.service";
import {BodyOutputType, Toast, ToasterService} from "angular2-toaster";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {


  emailId: string;
  password: string;

  constructor(public authService: AuthServiceService,
              private toasterService: ToasterService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  signup() {
    this.authService.signup(this.emailId, this.password);
    this.emailId = this.password = "";
  }

  login() {
    if (this.authService.login(this.emailId, this.password) === true) {
      const toast: Toast = {
        type: "success",
        title: "Authentication Success",
        body: "LoggedIn Successfully"
      };
      this.toasterService.popAsync(toast);
      const returnUrl = this.route.snapshot.queryParamMap.get("returnUrl");
      this.router.navigate([returnUrl || "/"]);
    } else {
      const toast: Toast = {
        type: "error",
        title: "Authentication Failed",
        body: "Invalid Credentials"
      };
      this.toasterService.popAsync(toast);
      this.emailId = this.password = "";
    }
  }

}
