import { UserService } from "./../users/shared/user.service";
import { Component, OnInit } from "@angular/core";
import { AuthServiceService } from "./shared/auth-service.service";
import { NgForm } from "@angular/forms/src/directives/ng_form";
import { Router } from "@angular/router";

@Component({
  selector: "app-authorization",
  templateUrl: "./authorization.component.html",
  styleUrls: ["./authorization.component.scss"],
})
export class AuthorizationComponent implements OnInit {
  email: string;
  password: string;

  constructor(public authService: AuthServiceService, private router: Router) {}

  ngOnInit() {}

  logout() {
    this.authService.logout();
    this.router.navigate(["login"]);
  }
}
