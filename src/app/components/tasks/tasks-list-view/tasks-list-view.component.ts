import { AuthServiceService } from "./../../authorization/shared/auth-service.service";
import { TaskService } from "./../shared/task.service";
import { Component, OnInit } from "@angular/core";
import { LoaderSpinnerService } from "../../../modules/loader-spinner/loader-spinner";
import { Task } from "../shared/task.model";

@Component({
  selector: "app-tasks-list-view",
  templateUrl: "./tasks-list-view.component.html",
  styleUrls: ["./tasks-list-view.component.scss"]
})
export class TasksListViewComponent implements OnInit {
  taskList: Task[] = [];
  search: string;
  task: Task = new Task();

  // Sorting
  key: string; // set default
  reverse = false;

  // Pagination
  p: number = 1;

  constructor(
    private taskService: TaskService,
    private spinnerService: LoaderSpinnerService,
    public authService: AuthServiceService
  ) {}

  ngOnInit() {
    this.key = "assignedDate";
    this.getAllTaks();
  }

  getAllTaks() {
    this.spinnerService.show();
    const x = this.taskService.getTasks();
    x.snapshotChanges().subscribe(task => {
      this.spinnerService.hide();
      this.taskList = [];
      task.forEach(element => {
        const y = element.payload.toJSON();
        y["$key"] = element.key;
        this.taskList.push(y as Task);
      });
    });
  }

  setOrder(value: string) {
    if (this.key === value) {
      this.reverse = !this.reverse;
    }

    this.key = value;
  }

  getTask(taskKey: string) {
    const x = this.taskService.getTaskById(taskKey);
    x.snapshotChanges().subscribe(element => {
      const y = element.payload.toJSON();
      y["$key"] = element.key;
      this.task = y;
    });
  }
}
