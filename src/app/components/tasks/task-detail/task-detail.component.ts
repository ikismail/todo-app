import { Task } from "./../shared/task.model";
import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-task-detail",
  templateUrl: "./task-detail.component.html",
  styleUrls: ["./task-detail.component.scss"]
})
export class TaskDetailComponent implements OnInit {
  @Input() task: Task = new Task();

  constructor() {}

  ngOnInit() {
    this.task = {
      $key: "",
      heading: "Task Heading",
      description: "Task Description",
      isCompleted: false,
      assignedDate: new Date(),
      assignedFor: "Assigned User",
      category: "Work"
    };
  }
}
