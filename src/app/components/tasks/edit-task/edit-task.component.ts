import { NgForm } from "@angular/forms/src/directives/ng_form";
import { Task } from "./../shared/task.model";
import { Component, OnInit, Input } from "@angular/core";
import { TaskService } from "../shared/task.service";
import { Router } from "@angular/router";
import { User } from "../../users/shared/user";
import { AuthServiceService } from "../../authorization/shared/auth-service.service";
import { LoaderSpinnerService } from "../../../modules/loader-spinner/loader-spinner";

declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-edit-task",
  templateUrl: "./edit-task.component.html",
  styleUrls: ["./edit-task.component.scss"]
})
export class EditTaskComponent implements OnInit {
  @Input() task: Task = new Task();
  @Input() userList: User[];
  category: string[] = ["Personal", "Work", "Home", "School"];
  isTaskChanged = false;

  constructor(
    private taskService: TaskService,
    private router: Router,
    private spinnerService: LoaderSpinnerService,
    public authService: AuthServiceService
  ) {}

  ngOnInit() {}

  updateTask(form: NgForm) {
    this.taskService.updateTask(form.value);
    $("#viewTask").modal("hide");
    this.router.navigate([""]);
  }
}
