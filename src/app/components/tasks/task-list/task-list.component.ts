import { UserService } from "./../../users/shared/user.service";
import { Component, OnInit } from "@angular/core";
import { Task } from "../shared/task.model";
import { TaskService } from "../shared/task.service";
import { forEach } from "@angular/router/src/utils/collection";
import { User } from "../../users/shared/user";
import { AuthServiceService } from "../../authorization/shared/auth-service.service";
import { ToasterService } from "angular2-toaster/src/toaster.service";
import { Toast } from "angular2-toaster/src/toast";
import { LoaderSpinnerService } from "../../../modules/loader-spinner/loader-spinner";

@Component({
  selector: "app-task-list",
  templateUrl: "./task-list.component.html",
  styleUrls: ["./task-list.component.scss"]
})
export class TaskListComponent implements OnInit {
  task: Task = new Task();
  taskList: Task[] = [];
  completedTask: Task[] = [];
  userList: User[];
  dragEnabled = false;

  // Charts
  // lineChart
  public lineChartOptions: any = {
    responsive: true
  };
  public lineChartData: Array<any> = [
    [65, 59, 80, 81, 56, 55, 40],
    [28, 48, 40, 19, 86, 27, 90]
  ];
  public lineChartLabels: Array<any> = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July"
  ];
  public lineChartType: string = "line";
  public pieChartType: string = "pie";
  // Pie
  public pieChartLabels: string[] = [
    "Download Sales",
    "In-Store Sales",
    "Mail Sales"
  ];
  public pieChartData: number[] = [300, 500, 100];
  // /Charts

  constructor(
    private taskService: TaskService,
    private spinnerService: LoaderSpinnerService,
    private userService: UserService,
    private toasterService: ToasterService,
    public authService: AuthServiceService
  ) {}

  ngOnInit() {
    this.getAllTaks();
    this.getAllUsers();
  }

  getAllTaks() {
    this.spinnerService.show();
    const x = this.taskService.getTasks();
    x.snapshotChanges().subscribe(task => {
      this.spinnerService.hide();
      this.taskList = [];
      this.completedTask = [];
      task.forEach(element => {
        const y = element.payload.toJSON();
        y["$key"] = element.key;
        if (y["isCompleted"]) {
          this.completedTask.push(y as Task);
        } else {
          this.taskList.push(y as Task);
        }
      });
    });
  }

  onDropFunction(e: any) {
    e.dragData["isCompleted"] = !e.dragData["isCompleted"];
    this.taskService.updateTask(e.dragData);
    this.getAllTaks();
    const toast: Toast = {
      type: "info",
      title: "Device Info",
      body: "Task Updated",
      timeout: 2000
    };

    this.toasterService.popAsync(toast);
  }

  getAllUsers() {
    const x = this.userService.getUsers();
    x.snapshotChanges().subscribe(user => {
      this.userList = [];
      user.forEach(element => {
        const y = element.payload.toJSON();
        y["$key"] = element.key;
        this.userList.push(y as User);
      });
    });
  }

  getTask(task: Task) {
    this.task = Object.assign({}, task);
  }

  // Charts
  public randomizeType(): void {
    this.lineChartType = this.lineChartType === "line" ? "bar" : "line";
    this.pieChartType = this.pieChartType === "doughnut" ? "pie" : "doughnut";
  }
  public chartClicked(e: any): void {
    console.log(e);
  }
  public chartHovered(e: any): void {
    console.log(e);
  }
}
