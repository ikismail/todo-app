export class Event {
  $key: string;
  title: string;
  startDate: Date;
  description: string;
  endDate: Date;
  color: string;
}
