import { NgForm } from "@angular/forms";
import { Event } from "./event";
import { TaskService } from "./../shared/task.service";
import { Component, OnInit } from "@angular/core";
import { AuthServiceService } from "../../authorization/shared/auth-service.service";
import * as moment from "moment";
import { DatePipe } from "@angular/common";
import { LoaderSpinnerService } from "../../../modules/loader-spinner/loader-spinner";

declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-task-schedule",
  templateUrl: "./task-schedule.component.html",
  styleUrls: ["./task-schedule.component.scss"]
})
export class TaskScheduleComponent implements OnInit {
  calendarOptions: Object;
  event = new Event();
  events: Event[];

  constructor(
    public authService: AuthServiceService,
    private taskService: TaskService,
    private spinnerService: LoaderSpinnerService,
    public datepipe: DatePipe
  ) {}

  ngOnInit() {
    this.configureCalendar();
  }

  configureCalendar() {
    this.getAllEvents();
    this.calendarOptions = {
      defaultDate: "2017-12-06",
      editable: true,
      eventLimit: true,
      customButtons: {
        myCustomButton: {
          text: "Add Event",
          click: function() {
            $("#eventModal").modal();
          }
        }
      },
      header: {
        left: "prev,next today myCustomButton",
        center: "title",
        right: "month,agendaWeek,agendaDay"
      },
      eventClick: function(calEvent, jsEvent, view) {
        alert("Event: " + calEvent.title);
      },
      eventRender: function(event, element) {
        $(element).tooltip({ title: event.description });
      }
    };
  }

  getAllEvents() {
    this.spinnerService.show();
    const x = this.taskService.getEvents();
    x.snapshotChanges().subscribe(event => {
      this.spinnerService.hide();
      this.events = [];
      event.forEach(element => {
        const y = element.payload.toJSON();
        y["$key"] = element.key;
        this.events.push(y as Event);
      });
    });
  }

  addEvent(event: NgForm) {
    console.log("addEvent", event);
    this.taskService.createEvent(event.value);
    $("#eventModal").modal("hide");
  }
}
