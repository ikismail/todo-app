import { DatePipe } from "@angular/common";
import { AngularFireList } from "angularfire2/database";
import { TaskService } from "./shared/task.service";
import { Component, OnInit } from "@angular/core";
import { Task } from "./shared/task.model";
import { UserService } from "../users/shared/user.service";

@Component({
  selector: "app-tasks",
  templateUrl: "./tasks.component.html",
  styleUrls: ["./tasks.component.scss"],
  providers: [TaskService, DatePipe]
})
export class TasksComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
