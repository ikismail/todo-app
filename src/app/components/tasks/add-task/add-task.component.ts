import { Router } from "@angular/router";
import { Task } from "./../shared/task.model";
import { Component, OnInit } from "@angular/core";
import { TaskService } from "../shared/task.service";
import { NgForm, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../users/shared/user.service";
import { User } from "../../Users/shared/user";
import { LoaderSpinnerService } from "../../../modules/loader-spinner/loader-spinner";

@Component({
  selector: "app-add-task",
  templateUrl: "./add-task.component.html",
  styleUrls: ["./add-task.component.scss"]
})
export class AddTaskComponent implements OnInit {
  taskFormGroup: FormGroup;

  task: Task;
  userList: User[];
  category: string[] = ["Personal", "Work", "Home", "School"];

  constructor(
    private service: TaskService,
    private spinnerService: LoaderSpinnerService,
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.getAllUsers();

    this.taskFormGroup = this.fb.group({
      "task-heading": [null, Validators.required],
      "task-description": [
        null,
        Validators.compose([
          Validators.required,
          Validators.minLength(30),
          Validators.maxLength(200)
        ])
      ],
      "task-assignedDate": [null, Validators.required],
      "task-category": [this.category[0]],
      "task-assignedFor": [null, Validators.required]
    });
  }

  ngOnInit() {}

  getAllUsers() {
    const x = this.userService.getUsers();
    x.snapshotChanges().subscribe(user => {
      this.userList = [];
      user.forEach(element => {
        const y = element.payload.toJSON();
        y["$key"] = element.key;
        this.userList.push(y as User);
      });
    });
  }

  addTask(form) {
    this.task = {
      $key: "",
      heading: form["task-heading"],
      description: form["task-description"],
      category: form["task-category"],
      assignedFor: form["task-assignedFor"],
      assignedDate: form["task-assignedDate"],
      isCompleted: false
    };
    this.service.createTask(this.task);
    this.router.navigate(["task-list"]);
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.reset();
    }
  }
}
