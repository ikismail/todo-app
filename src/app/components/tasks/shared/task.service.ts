import { Task } from "./task.model";
import { Injectable } from "@angular/core";
import {
  AngularFireDatabase,
  AngularFireList,
  AngularFireObject
} from "angularfire2/database";
import { Http } from "@angular/http";
import { Event } from "../task-schedule/event";

@Injectable()
export class TaskService {
  selectedTask: Task = new Task();

  task: AngularFireObject<any>;
  tasks: AngularFireList<any>;
  events: AngularFireList<any>;
  event: AngularFireObject<any>;

  // events: any = [
  //   {
  //     id: 1,
  //     title: "All Day Event",
  //     start: "2017-12-01"
  //   },
  //   {
  //     id: 2,
  //     title: "Long Event",
  //     start: "2017-12-07",
  //     end: "2017-12-10"
  //   },
  //   {
  //     id: 3,
  //     title: "Repeating Event",
  //     start: "2017-12-09T16:00:00"
  //   },
  //   {
  //     id: 4,
  //     title: "Repeating Event",
  //     start: "2017-12-16T16:00:00"
  //   },
  //   {
  //     id: 5,
  //     title: "Conference",
  //     start: "2017-12-11",
  //     end: "2017-12-13"
  //   },
  //   {
  //     id: 6,
  //     title: "Meeting",
  //     start: "2017-12-12T10:30:00",
  //     end: "2017-12-12T12:30:00"
  //   },
  //   {
  //     id: 7,
  //     title: "Lunch",
  //     start: "2017-12-06T12:00:00"
  //   },
  //   {
  //     id: 8,
  //     title: "Meeting",
  //     start: "2017-12-12T14:30:00"
  //   },
  //   {
  //     id: 9,
  //     title: "Happy Hour",
  //     start: "2017-12-12T17:30:00"
  //   },
  //   {
  //     id: 10,
  //     title: "Dinner",
  //     start: "2017-12-12T20:00:00"
  //   },
  //   {
  //     id: 11,
  //     title: "Birthday Party",
  //     start: "2017-12-13T07:00:00"
  //   },
  //   {
  //     id: 12,
  //     title: "Click for Google",
  //     url: "http://google.com/",
  //     start: "2017-12-28",
  //     color: "red"
  //   }
  // ];

  constructor(private db: AngularFireDatabase, private http: Http) {}

  getTasks() {
    this.tasks = this.db.list("tasks");
    return this.tasks;
  }

  getEvents() {
    this.events = this.db.list("events");
    return this.events;
  }

  createTask(task: Task) {
    this.tasks.push({
      heading: task.heading,
      description: task.description,
      category: task.category,
      isCompleted: (task.isCompleted = false),
      assignedFor: task.assignedFor,
      assignedDate: task.assignedDate
    });
  }

  createEvent(event: Event) {
    if (event.endDate === undefined) {
      event.endDate = null;
    }
    this.events.push({
      title: event.title,
      start: event.startDate,
      description: event.description,
      end: event.endDate,
      color: event.color
    });
  }
  getTaskById(key: string): any {
    this.task = this.db.object("tasks/" + key);
    return this.task;
  }
  getEventById(key: string) {
    this.event = this.db.object("events/" + key);
    return this.event;
  }

  updateTask(task: Task) {
    this.tasks.update(task.$key, {
      heading: task.heading,
      description: task.description,
      category: task.category,
      isCompleted: task.isCompleted,
      assignedFor: task.assignedFor,
      assignedDate: task.assignedDate
    });
  }

  updateEvent(event: Event) {
    this.events.update(event.$key, {
      title: event.title,
      start: event.startDate,
      description: event.description,
      end: event.endDate,
      color: event.color
    });
  }

  deleteTask(key: string) {
    this.tasks.remove(key);
  }

  deleteEvent(key: string) {
    this.events.remove(key);
  }
}
