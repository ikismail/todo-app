export class Task {
    $key: string;
    heading: string;
    description: string;
    assignedFor: string;
    category: string;
    isCompleted: boolean;
    assignedDate: Date;
}
