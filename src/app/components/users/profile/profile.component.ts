import { AuthServiceService } from "./../../authorization/shared/auth-service.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent implements OnInit {
  constructor(public authService: AuthServiceService) {}

  ngOnInit() {}
}
