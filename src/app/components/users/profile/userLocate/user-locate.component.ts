import { UserService } from "./../../shared/user.service";
import { Component, OnInit } from "@angular/core";
import { User } from "../../shared/user";

@Component({
  selector: "app-user-locate",
  templateUrl: "./user-locate.component.html",
  styleUrls: ["./user-locate.component.scss"]
})
export class UserLocateComponent implements OnInit {
  // google maps zoom level

  zoom = 10;
  scrollWheel = true;
  zoomControl = true;
  // icon = "https://cdn2.iconfinder.com/data/icons/IconsLandVistaMapMarkersIconsDemo/32/MapMarker_Bubble_Azure.png";

  // info Window
  isOpen = false;
  infoContent: string;

  locations: User[];
  events: any[];

  // Data
  viewData = false;
  // initial center position for the map
  lat = 13.1484124;
  lng = 80.1745727;

  constructor(private userService: UserService) {}

  ngOnInit() {
    const x = this.userService.getUsers();
    x.snapshotChanges().subscribe(user => {
      this.locations = [];
      let i = 0;
      user.forEach(element => {
        const y = element.payload.toJSON();
        const charCode = "A";
        y["$key"] = element.key;
        y["label"] = String.fromCharCode(charCode.charCodeAt(0) + i++);
        this.locations.push(y as User);
      });
    });
  }

  clickedMarker(location: User) {
    this.isOpen = !this.isOpen;
    if (this.isOpen) {
      this.isOpen = !this.isOpen;
    }
    // this.isOpen = this.isOpen;
    this.infoContent = location.userName;
    const events: any = location["events"];

    this.events = [];
    for (const key in events) {
      if (events.hasOwnProperty(key)) {
        this.events.push(events[key]);
      }
    }
    this.viewData = true;
  }
}
