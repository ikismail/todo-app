import { NgForm, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { AuthServiceService } from "../../../authorization/shared/auth-service.service";
import { User } from "../../shared/user";

@Component({
  selector: "app-account",
  templateUrl: "./account.component.html",
  styleUrls: ["./account.component.scss"]
})
export class AccountComponent implements OnInit {
  loggedUser: User;
  // Enable Update Button
  enbUpdBut: Boolean = true;

  constructor(
    private authService: AuthServiceService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.getProfile();
  }

  getProfile() {
    this.loggedUser = this.authService.getLoggedInUser();
  }

  updateProfile(form: NgForm) {}
}
