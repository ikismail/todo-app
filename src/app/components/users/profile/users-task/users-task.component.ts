import { User } from "./../../shared/user";
import { Task } from "./../../../tasks/shared/task.model";
import { TaskService } from "./../../../tasks/shared/task.service";
import { Component, OnInit } from "@angular/core";
import { AuthServiceService } from "../../../authorization/shared/auth-service.service";

@Component({
  selector: "app-users-task",
  templateUrl: "./users-task.component.html",
  styleUrls: ["./users-task.component.scss"],
  providers: [TaskService]
})
export class UsersTaskComponent implements OnInit {
  usersTasks: Task[];
  currentUserName: string;
  constructor(
    private taskService: TaskService,
    private authService: AuthServiceService
  ) {}

  ngOnInit() {
    const currentUser = this.authService.getLoggedInUser();
    this.currentUserName = currentUser["userName"];
    this.getTasks();
  }

  getTasks() {
    // Task List
    const x = this.taskService.getTasks();
    x.snapshotChanges().subscribe(task => {
      this.usersTasks = [];
      task.forEach(element => {
        const y = element.payload.toJSON();
        if (y["assignedFor"] === this.currentUserName) {
          this.usersTasks.push(y as Task);
        }
      });
    });
  }
}
