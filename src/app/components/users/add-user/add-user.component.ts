import { Router } from "@angular/router";
import { Component, OnInit, Input } from "@angular/core";
import { UserService } from "../shared/user.service";
import { User } from "../shared/user";
import { NgForm } from "@angular/forms/src/directives/ng_form";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"]
})
export class AddUserComponent implements OnInit {
  @Input() showForm: boolean;
  user: User = new User();

  constructor(public service: UserService, private router: Router) {}

  ngOnInit() {
    this.resetForm();
  }

  addUser(form: NgForm) {
    this.service.createUser(form.value);
    this.resetForm(form);
    this.router.navigate(["/user-list"]);
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.reset();
    }
  }
}
