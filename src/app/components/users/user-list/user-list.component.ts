import { UserService } from "./../shared/user.service";
import { Component, OnInit } from "@angular/core";
import { User } from "../shared/user";
import { AuthServiceService } from "../../authorization/shared/auth-service.service";
import { LoaderSpinnerService } from "../../../modules/loader-spinner/loader-spinner";
import { document } from "angular-bootstrap-md/utils/facade/browser";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"]
})
export class UserListComponent implements OnInit {
  showForm: Boolean = false;
  userList: User[];

  // Search Users
  query: string;
  filteredList: any[] = [];
  showError = false;
  labelActive = false;

  // test
  states = [
    "Alabama",
    "Alaska",
    "Arizona",
    "Arkansas",
    "California",
    "Colorado"
  ];

  constructor(
    private userService: UserService,
    public authService: AuthServiceService,
    private spinnerService: LoaderSpinnerService
  ) {}

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers() {
    this.spinnerService.show();
    const x = this.userService.getUsers();
    x.snapshotChanges().subscribe(user => {
      this.spinnerService.hide();
      this.userList = [];
      user.forEach(element => {
        const y = element.payload.toJSON();
        y["$key"] = element.key;
        this.userList.push(y as User);
      });
    });
  }

  // Filter
  filter(event) {
    console.log("Event", event);
    if (
      (event.keyCode > 64 && event.keyCode < 91) ||
      (event.keyCode > 96 && event.keyCode < 123) ||
      event.keyCode === 8
    ) {
      console.log("inside if");
      if (this.query !== "") {
        this.showError = false;
        const x = this.userService.getUsersByUserName(this.query);
        x.snapshotChanges().subscribe(user => {
          this.filteredList = [];
          user.forEach(element => {
            const y = element.payload.toJSON();
            this.filteredList.push(y as User);
          });
        });
      } else {
        this.showError = true;
        this.filteredList = [];
      }
    }
  }
  select(item: any) {
    this.query = item["userName"];
    this.labelActive = true;
    this.filteredList = [];
  }
}
