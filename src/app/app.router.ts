import { TasksListViewComponent } from "./components/tasks/tasks-list-view/tasks-list-view.component";
import { TaskScheduleComponent } from "./components/tasks/task-schedule/task-schedule.component";
import { UsersTaskComponent } from "./components/users/profile/users-task/users-task.component";
import { AuthorizationComponent } from "./components/authorization/authorization.component";
import { LoginComponent } from "./components/authorization/login/login.component";
import { UserListComponent } from "./components/users/user-list/user-list.component";
import { TasksComponent } from "./components/tasks/tasks.component";
import { Routes } from "@angular/router";
import { AddTaskComponent } from "./components/tasks/add-task/add-task.component";
import { TaskListComponent } from "./components/tasks/task-list/task-list.component";
import { UsersComponent } from "./components/users/users.component";
import { AuthGuard } from "./components/authorization/shared/auth_gaurd";
import { NoAccessComponent } from "./components/common/no-access/no-access.component";
import { PageNotFoundComponent } from "./components/common/page-not-found/page-not-found.component";
import { AdminGaurd } from "./components/authorization/shared/admin-gaurd";
import { ProfileComponent } from "./components/users/profile/profile.component";
import { AccountComponent } from "./components/users/profile/account/account.component";
import { UserLocateComponent } from "./components/users/profile/userLocate/user-locate.component";
import { TaskDetailComponent } from "./components/tasks/task-detail/task-detail.component";

export const appRoutes: Routes = [
  { path: "", redirectTo: "task-list", pathMatch: "full" },
  {
    path: "task-list",
    component: TaskListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "task-list-list",
    component: TasksListViewComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "task-list/:id",
        component: TaskDetailComponent,
        outlet: "taskDetails"
      }
    ]
  },
  {
    path: "task-schedule",
    component: TaskScheduleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "add-task",
    component: AddTaskComponent,
    canActivate: [AuthGuard, AdminGaurd]
  },
  { path: "login", component: LoginComponent },
  {
    path: "user-list",
    component: UsersComponent,
    canActivate: [AuthGuard, AdminGaurd]
  },
  {
    path: "users",
    component: ProfileComponent,
    children: [
      { path: "", component: AccountComponent, outlet: "profileOutlet" },
      {
        path: "locate-users",
        component: UserLocateComponent,
        outlet: "profileOutlet",
        canActivate: [AuthGuard, AdminGaurd]
      },
      {
        path: "users-task",
        component: UsersTaskComponent,
        outlet: "profileOutlet",
        canActivate: [AuthGuard]
      }
    ]
  },
  { path: "no-access", component: NoAccessComponent },
  { path: "**", component: PageNotFoundComponent }
];
