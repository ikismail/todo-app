import { OrderPipe } from "./components/tasks/shared/orderBy.pipe";
import { SearchPipe } from "./components/tasks/shared/filter.pipe";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  NgModule,
  Component,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA
} from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { HttpModule } from "@angular/http";
import { LoaderSpinnerModule } from "./modules/loader-spinner/loader-spinner.module";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { AngularFireModule } from "angularfire2";
import { AngularFireAuthModule } from "angularfire2/auth";
import { ToasterModule } from "angular2-toaster";
import { Ng2DeviceDetectorModule } from "ng2-device-detector";
import { NotificationsModule } from "angular-notice";
import { Ng2DragDropModule } from "ng2-drag-drop";
import { AgmCoreModule } from "@agm/core";
import { ScheduleModule } from "primeng/primeng";
import {
  DataTableModule,
  SharedModule,
  AutoCompleteModule
} from "primeng/primeng";
import { NgxPaginationModule } from "ngx-pagination";
import { DndModule } from "ng2-dnd";
import { ChartsModule } from "ng2-charts";

// Configuration Import
import { FireBaseConfig } from "../environments/firebaseConfig";
import { appRoutes } from "../app/app.router";
import { MAP_API_KEY } from "../environments/environment";
// Component Imports
import { AppComponent } from "./app.component";
import { TasksComponent } from "./components/tasks/tasks.component";
import { UsersComponent } from "./components/users/users.component";
import { EditTaskComponent } from "./components/tasks/edit-task/edit-task.component";
import { NavbarComponent } from "./components/common/navbar/navbar.component";
import { AddTaskComponent } from "./components/tasks/add-task/add-task.component";
import { TaskListComponent } from "./components/tasks/task-list/task-list.component";
import { AddUserComponent } from "./components/users/add-user/add-user.component";
import { UserListComponent } from "./components/users/user-list/user-list.component";
import { AuthorizationComponent } from "./components/authorization/authorization.component";
import { LoginComponent } from "./components/authorization/login/login.component";
import { AuthServiceService } from "./components/authorization/shared/auth-service.service";
import { AuthGuard } from "./components/authorization/shared/auth_gaurd";
import { NoAccessComponent } from "./components/common/no-access/no-access.component";
import { PageNotFoundComponent } from "./components/common/page-not-found/page-not-found.component";
import { AdminGaurd } from "./components/authorization/shared/admin-gaurd";
import { UserService } from "./components/users/shared/user.service";
import { ProfileComponent } from "./components/users/profile/profile.component";
import { AccountComponent } from "./components/users/profile/account/account.component";
import { UserLocateComponent } from "./components/users/profile/userLocate/user-locate.component";
import { FooterModule } from "./modules/footer/footer.module";
import { UsersTaskComponent } from "./components/users/profile/users-task/users-task.component";
import { TaskScheduleComponent } from "./components/tasks/task-schedule/task-schedule.component";
import { TasksListViewComponent } from "./components/tasks/tasks-list-view/tasks-list-view.component";
import { TaskDetailComponent } from "./components/tasks/task-detail/task-detail.component";

@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    UsersComponent,
    EditTaskComponent,
    NavbarComponent,
    AddTaskComponent,
    TaskListComponent,
    AddUserComponent,
    UserListComponent,
    AuthorizationComponent,
    LoginComponent,
    NoAccessComponent,
    PageNotFoundComponent,
    ProfileComponent,
    AccountComponent,
    UserLocateComponent,
    UsersTaskComponent,
    TaskScheduleComponent,
    TasksListViewComponent,
    SearchPipe,
    OrderPipe,
    TaskDetailComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    LoaderSpinnerModule,
    AngularFireModule.initializeApp(FireBaseConfig),
    AngularFireDatabaseModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    Ng2DeviceDetectorModule.forRoot(),
    AngularFireAuthModule,
    ToasterModule,
    NotificationsModule,
    Ng2DragDropModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: MAP_API_KEY
    }),
    FooterModule,
    ScheduleModule,
    DataTableModule,
    SharedModule,
    NgxPaginationModule,
    DndModule.forRoot(),
    ChartsModule
  ],
  providers: [
    NotificationsModule,
    AuthServiceService,
    AuthGuard,
    AdminGaurd,
    UserService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {}
