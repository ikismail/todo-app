import { ToasterConfig } from "angular2-toaster";
export const environment = {
  production: true
};

export const toasterconfig = new ToasterConfig({
  animation: "fade",
  positionClass: "toast-top-right",
  tapToDismiss: true
});

export const MAP_API_KEY = "AIzaSyDMbxW3MlwUP2vrAZVJyu7pYqZa1LthvTE";
